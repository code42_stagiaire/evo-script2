﻿###################################################################################################################
#
#   nom du script   : CLIENT_JOIN.PS1
#   description     : Permet de  
#                           JOINDRE UN DOMAINE
#                           80- Joindre le domaine
#                           81- Envoie Email                   
#                           82- Redemarrage de la VM
#   teste sur       : Windows 2012R2
#   auteur          : Geoffroy CORBINEAU
#   statut          : Développement
#   version         : 0.7
#   
#
###################################################################################################################


###################################################################################################################
#
#   Variables
#
###################################################################################################################

$DC1_nom_dom = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_nom_dom" 2>&1 | Out-String
$DC1_nom_dom = $DC1_nom_dom -replace "`t|`n|`r",""
$adminpass = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.adminpass" 2>&1 | Out-String
$adminpass = $adminpass -replace "`t|`n|`r",""
$adminpass = ConvertTo-SecureString -AsPlainText -String $adminpass -Force

$username = "$DC1_nom_dom\admin"
$credential = New-Object System.Management.Automation.PSCredential($username,$adminpass)
$log = "C:\SCRIPTS\Logs\logs.txt"


###################################################################################################################
#
#   Main
#
###################################################################################################################

###80- Joindre le domaine##########################################################################################

$Error.clear()
echo `t "###CLIENT_JOIN.PS1###########" >> $log
echo "80- Joindre le domaine $DC1_nom_dom :" >> $log
Add-Computer -DomainName $DC1_nom_dom -Credential $credential
#New-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "Read_role" -Value "$pshome\powershell.exe -file C:\SCRIPTS\Scripts_PS\READ_ROLE.ps1" -Force
$Error.Exception.Message >> $log

###81- Envoie Email################################################################################################

echo `t "82- Redemarrage de la VM :" >> $log
date >> $log
$logs = cat $log 
$email = @{
                    To = "stagiaire@code42.fr","alerte@code42.fr"
                    From = "evo@code42.io"
                    Subject = "[EVO] - CONfiguration rejoint domaine -> $VM_name"
                    Body = ( $logs | Out-String )
                    SmtpServer = "f.code42.fr"
                    port = "25"
               }
                    Send-MailMessage @email 
###82- Redémarrage de la VM########################################################################################

Restart-Computer