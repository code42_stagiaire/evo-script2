###################################################################################################################
#
#   nom du script   : CUSTOM.PS1 
#   description     : Permet de  
#                           1- Cr�ation des dossiers 
#                           2- Importation Unattend.xml
#                           3- Importation des scripts
#                           4- Importation des logiciels
#                           5- Installation des logiciels
#                           6- Afficher les extensions de fichiers, fichier systeme et fichier cach�
#                           7- Desactiver securit� renforc�e IE
#                           20- Ajout du fichier maj dans runonce
#                           21- Recuperation du sid
#                           22- Redemarrage de la VM 
#   teste sur       : Windows 7,10,2008R2,2012R2,2016
#   auteur          : Geoffroy CORBINEAU
#   statut          : D�veloppement
#   version         : 0.7
#   
#
###################################################################################################################


###################################################################################################################
#
#   Variables
#
###################################################################################################################

$os = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.os" 2>&1 | Out-String
$os = $os -replace "`t|`n|`r",""
$addresse = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.addresse" 2>&1 | Out-String
$addresse = $addresse -replace "`t|`n|`r",""
$sources = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.sources" 2>&1 | Out-String
$sources = $sources -replace "`t|`n|`r",""

$log = "C:\SCRIPTS\Logs\logs.txt"
$sysinternalssuite = "SysinternalsSuite.zip"
$adobeflash = "install_flash_player_latest.exe"
$adobereader = "AcroRdrDC-latest.exe"
$jre = 'jre-latest.exe'
$zabbix = "zabbix_agent-latest.msi"
$cygwin = "Cygwin.7z" 
$7zip = "7z-latest.exe"
$salt = "Salt-Minion-latest.exe"
$bginfo = "bginfo.bgi"
$google = "google.reg"
$wc = New-Object System.Net.WebClient
$customexists = Test-Path C:\SCRIPTS\Scripts_PS\CUSTOM.ps1

###################################################################################################################
#
#   Fonctions (1- Permet de recuperer des infos sur l'OS)
#
###################################################################################################################

Function Get-OSInfo
{
   param ([String]$ComputerName = '.')

   $infos = Get-WmiObject Win32_OperatingSystem -ComputerName $ComputerName

   $infos | Select-Object -property @{Name='OS'; Expression = {$_.caption}},
                                    @{Name='ServicePack'; Expression = {$_.csdversion}},
                                    @{Name='InstallationDate'; Expression ={[system.Management.ManagementDateTimeConverter]::ToDateTime($_.Installdate)}}
                                    
}

###################################################################################################################
#
#   Main
#
###################################################################################################################


###Test si le role custom a deja ete effectuer#####################################################################


If ($customexists -eq $true) {
echo "LE ROLE CUSTOM.PS1 A DEJA ETE EXECUTER (Veuillez changer la valeur guestinfo.role) EXIT" >> $log 
exit
}


###1- Cr�ation des dossiers########################################################################################

$Error.clear()
mkdir C:\INSTALL
mkdir C:\INSTALL\SOURCES
mkdir C:\INSTALL\SysinternalsSuite
mkdir C:\SCRIPTS
mkdir C:\SCRIPTS\Logs
mkdir C:\SCRIPTS\Scripts_PS
mkdir C:\Windows\Setup\Scripts
Get-OSInfo | Format-List > $log
echo "###CUSTOM.PS1################" >> $log
echo "1- Creation des dossiers :" >> $log
$Error.Exception.Message >> $log

###2- Importation Unattend.xml####################################################################################

$Error.clear()
echo "2- Importation Unattend.xml :" >> $log
If ($os -match "WIN7") {
Invoke-WebRequest $addresse/UnattendWIN7.xml -OutFile C:\Windows\System32\Sysprep\Unattend.xml
}
If ($os -match "WIN10") {
Invoke-WebRequest $addresse/UnattendWIN10.xml -OutFile C:\Windows\System32\Sysprep\Unattend.xml
}
If ($os -match "WIN2008") {
Invoke-WebRequest $addresse/UnattendWIN2008.xml -OutFile C:\Windows\System32\Sysprep\Unattend.xml
}
If ($os -match "WIN2012") {
Invoke-WebRequest $addresse/UnattendWIN2012.xml -OutFile C:\Windows\System32\Sysprep\Unattend.xml
}
If ($os -match "WIN2016") {
Invoke-WebRequest $addresse/UnattendWIN2016.xml -OutFile C:\Windows\System32\Sysprep\Unattend.xml
}
$Error.Exception.Message >> $log

###3- Importation des scripts######################################################################################

$Error.clear()
echo "3- Importation des scripts :" >> $log 
Invoke-WebRequest $addresse/Setupcomplete.cmd -OutFile C:\Windows\Setup\Scripts\Setupcomplete.cmd 
Invoke-WebRequest $addresse/DL_SCOMPLETE.ps1 -OutFile C:\SCRIPTS\Scripts_PS\DL_SCOMPLETE.ps1
Invoke-WebRequest $addresse/MAJ.ps1 -OutFile C:\SCRIPTS\Scripts_PS\MAJ.ps1
Invoke-WebRequest $addresse/SYSPREP.cmd -OutFile C:\SCRIPTS\Scripts_PS\SYSPREP.cmd
Invoke-WebRequest $addresse/SYSPREP.ps1 -OutFile C:\SCRIPTS\Scripts_PS\SYSPREP.ps1
Invoke-WebRequest $addresse/READ_ROLE.ps1 -OutFile C:\SCRIPTS\Scripts_PS\READ_ROLE.ps1
Invoke-Webrequest $addresse/CUSTOM.ps1 -Outfile C:\SCRIPTS\Scripts_PS\CUSTOM.ps1
$Error.Exception.Message >> $log

###4- Importation des logiciels####################################################################################

$Error.clear()
echo "4- Importation des logiciels :" >> $log
$wc.DownloadFile("$sources/$sysinternalssuite", "C:\INSTALL\SOURCES\$sysinternalssuite")
$wc.DownloadFile("$sources/$7zip", "C:\INSTALL\SOURCES\$7zip")
$wc.DownloadFile("$sources/$adobeflash", "C:\INSTALL\SOURCES\$adobeflash")
$wc.DownloadFile("$sources/$adobereader", "C:\INSTALL\SOURCES\$adobereader")
$wc.DownloadFile("$sources/$jre", "C:\INSTALL\SOURCES\$jre")
$wc.DownloadFile("$sources/$cygwin", "C:\INSTALL\SOURCES\$cygwin")
$wc.DownloadFile("$sources/$zabbix", "C:\INSTALL\SOURCES\$zabbix")
$wc.DownloadFile("$sources/$salt", "C:\INSTALL\SOURCES\$salt")
$wc.DownloadFile("$sources/$google", "C:\INSTALL\SOURCES\$google")
$wc.DownloadFile("$sources/$bginfo", "C:\INSTALL\SysinternalsSuite\bginfo.bgi")
$Error.Exception.Message >> $log

###5- Installation des logiciels###################################################################################

$Error.clear()
echo "5- Installation des logiciels :" >> $log
cmd.exe /C "C:\INSTALL\SOURCES\$adobereader" "/sAll" | Out-Null
cmd.exe /C "C:\INSTALL\SOURCES\$7zip" "/S" | Out-Null
cmd.exe /C "C:\INSTALL\SOURCES\$jre" "/s" | Out-Null
cmd.exe /C "C:\Program Files\7-Zip\7z.exe" "x" "C:\INSTALL\SOURCES\$sysinternalssuite" "-oC:\INSTALL\SysinternalsSuite" | Out-Null
cmd.exe /C "C:\Program Files\7-Zip\7z.exe" "x" "C:\INSTALL\SOURCES\$cygwin" "-oC:\INSTALL\Cygwin" | Out-Null
cmd.exe /C "C:\INSTALL\Cygwin\scripts\cygwin_distrib_install.cmd" | Out-Null
regedit /s C:\INSTALL\SOURCES\$google
If (($os -match "WIN7") -or ($os -match "WIN2008")) { 
cmd.exe /C "C:\INSTALL\SOURCES\$adobeflash" "-install" | Out-Null
}
$Error.Exception.Message >> $log


###6- Afficher les extensions de fichiers, fichier systeme et fichier cach�########################################

$Error.clear()
echo "6- Afficher les extensions de fichiers, fichier systeme et fichier cache :" >> $log
$key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $key Hidden 1
Set-ItemProperty $key HideFileExt 0
Set-ItemProperty $key ShowSuperHidden 1
Stop-Process -processname explorer
$Error.Exception.Message >> $log

###7- Desactiver securit� renforc�e IE :###########################################################################

If (($os -match "WIN2008") -or ($os -match "WIN2012") -or ($os -match "WIN2016")) {
$Error.clear()
echo "7- Desactiver securite renforcee IE :" >> $log
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Internet Explorer\Main\ESCHomePages" -Name "SoftAdmin" -Value "http://www.google.com"
Rundll32 iesetup.dll, IEHardenLMSettings
Rundll32 iesetup.dll, IEHardenUser
Rundll32 iesetup.dll, IEHardenAdmin
$Error.Exception.Message >> $log
}

###20- Ajout du fichier maj dans runonce puis reboot###############################################################

$Error.clear()
echo "20- Ajout du fichier MAJ.PS1 dans runonce :" >> $log
New-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "maj" -Value "$pshome\powershell.exe -file C:\SCRIPTS\Scripts_PS\MAJ.ps1" -Force
$Error.Exception.Message >> $log

###21- Recuperation du sid#########################################################################################

$Error.clear()
echo "21- Recuperation du sid puis Restart:" >> $log
echo "22- Redemarrage de la VM :" >> $log
C:\INSTALL\SysinternalsSuite\PsGetsid.exe /accepteula >> $log
$Error.Exception.Message >> $log

###22- Red�marrage de la VM########################################################################################

Restart-Computer

