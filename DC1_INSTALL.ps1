﻿###################################################################################################################
#
#   nom du script   : DC1_INSTALL.PS1
#   description     : Permet de  
#                           INSTALLATION DU ROLE CONTROLEUR DE DOMAINE
#                           80- Envoyer les guestinfo dans logs.txt
#                           81- Installation des rôles prérequis
#                           82- Installation ADDSForest 
#                           83- Configuration DNS
#                           84- Envoie Email
#                           85- Redémarrage de la VM
#   teste sur       : Windows 2012R2
#   auteur          : Geoffroy CORBINEAU
#   statut          : Développement
#   version         : 0.7
#   
#
###################################################################################################################
#
#$DC1_niv_foret = "Win2012"
#$DC1_niv_dom = "Win2012"
#     -- Windows Server 2003: 2 or Win2003
#     -- Windows Server 2008: 3 or Win2008
#     -- Windows Server 2008 R2: 4 or Win2008R2
#     -- Windows Server 2012: 5 or Win2012
#     -- Windows Server 2012 R2: 6 or Win2012R2
#
#$DC1_nom_dom = "domaine.test"
#$DC1_nom_dom_NB = "DOMAINETEST"
#$DC1_safeadminpass = "Mot de passe de recovery AD"
#$DC1_install_dns = "True" or "False" (default : True)
#$DC1_deleg_dns = "True" or "False" (default : False)
#
###################################################################################################################
#
#   Variables
#
###################################################################################################################

$DC1_niv_foret = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_niv_foret" 2>&1 | Out-String
$DC1_niv_foret = $DC1_niv_foret -replace "`t|`n|`r",""
$DC1_niv_dom = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_niv_dom" 2>&1 | Out-String
$DC1_niv_dom = $DC1_niv_dom -replace "`t|`n|`r",""
$DC1_nom_dom = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_nom_dom" 2>&1 | Out-String
$DC1_nom_dom = $DC1_nom_dom -replace "`t|`n|`r",""
$DC1_nom_dom_NB = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_nom_dom_NB" 2>&1 | Out-String
$DC1_nom_dom_NB = $DC1_nom_dom_NB -replace "`t|`n|`r",""
$DC1_install_dns = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_install_dns" 2>&1 | Out-String
$DC1_install_dns = $DC1_install_dns -replace "`t|`n|`r",""
$DC1_deleg_dns = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_deleg_dns" 2>&1 | Out-String
$DC1_deleg_dns = $DC1_deleg_dns -replace "`t|`n|`r",""
$DC1_safeadminpass = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_safeadminpass" 2>&1 | Out-String
$DC1_safeadminpass = $DC1_safeadminpass -replace "`t|`n|`r",""
$DC1_safeadminpass = ConvertTo-SecureString -AsPlainText -String $DC1_safeadminpass -Force

$log = "C:\SCRIPTS\Logs\logs.txt" 


###################################################################################################################
#
#   Main
#
###################################################################################################################

###80- Envoyer les guestinfo dans logs.txt#########################################################################

$Error.clear()
echo `t "###DC1_INSTALL.PS1###########" `t >> $log
echo "80- Lecture guestinfo :" `
"guestinfo.DC1_niv_foret = $DC1_niv_foret" `
"guestinfo.DC1_niv_dom = $DC1_niv_dom" `
"guestinfo.DC1_nom_dom = $DC1_nom_dom" `
"guestinfo.DC1_nom_dom_NB = $DC1_nom_dom_NB" `
"guestinfo.DC1_safeadminpass = $DC1_safeadminpass" `
"guestinfo.DC1_install_dns = $DC1_install_dns" `
"guestinfo.DC1_deleg_dns = $DC1_deleg_dns" >> $log
$Error.Exception.Message >> $log

###81- Installation des rôles prérequis############################################################################

$Error.clear()
echo "81- Installation des roles prerequis pour le domaine $DC1_nom_dom :" >> $log
Install-WindowsFeature RSAT-ADDS
Install-WindowsFeature AD-Domain-Services -IncludeManagementTools 
$Error.Exception.Message >> $log

###82- Installation ADDSForest + Adcs-Cert-Authority###############################################################

$Error.clear()
echo "82- Installation ADDSForest + Adcs-Cert-Authority :" >> $log
Install-ADDSForest `
-DatabasePath 'C:\Windows\NTDS' `
-DomainMode $DC1_niv_dom `
-DomainName $DC1_nom_dom `
-DomainNetbiosName $DC1_nom_dom_NB `
-ForestMode $DC1_niv_foret `
-Force:$true `
-CreateDnsDelegation:([System.Convert]::ToBoolean($DC1_deleg_dns)) `
-LogPath 'C:\Windows\NTDS' `
-NoRebootOnCompletion:$true `
-SysvolPath 'C:\Windows\SYSVOL' `
-InstallDns:([System.Convert]::ToBoolean($DC1_install_dns)) `
-SafeModeAdministratorPassword $DC1_safeadminpass
Install-WindowsFeature Adcs-Cert-Authority
$Error.Exception.Message >> $log

###83- Configuration DNS###########################################################################################

$Error.clear()
echo "83- Configuration DNS :" >> $log
Add-DnsServerForwarder -IPAddress 8.8.8.8 -PassThru
New-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "reverse_dns_zone" -Value "$pshome\powershell.exe -file C:\SCRIPTS\Scripts_PS\REVERSE_DNS.ps1" -Force
$Error.Exception.Message >> $log

###84- Envoie Email################################################################################################

echo `t "85- Redemarrage de la VM :" >> $log
date >> $log
$logs = cat $log 
$email = @{
                    To = "stagiaire@code42.fr","alerte@code42.fr"
                    From = "evo@code42.io"
                    Subject = "[EVO] - CONfiguration role DC rapport -> $VM_name"
                    Body = ( $logs | Out-String )
                    SmtpServer = "f.code42.fr"
                    port = "25"
               }
                    Send-MailMessage @email 
					
###85- Redémarrage de la VM########################################################################################

Restart-Computer
