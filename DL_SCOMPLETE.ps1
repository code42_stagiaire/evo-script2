﻿###################################################################################################################
#
#   nom du script   : DL_SCOMPLETE.PS1
#   description     : Permet de  
#                           0- Lecture des guestinfo
#                           0- Telechargement Setupcomplete.ps1 
#   teste sur       : Windows 7,10,2008R2,2012R2,2016
#   auteur          : Geoffroy CORBINEAU
#   statut          : Développement
#   version         : 0.7
#   
#
###################################################################################################################


###0- Lecture des guestinfo########################################################################################

$addresse = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.addresse" 2>&1 | Out-String
$addresse = $addresse -replace "`t|`n|`r",""
$log = "C:\SCRIPTS\Logs\logs.txt" 

###0- Telechargement###############################################################################################

$Error.clear()
echo `t "###DL_SCOMPLETE.PS1##########" >> $log
echo "39- Importation Setupcomplete.ps1 :" >> $log
Invoke-WebRequest $addresse/Setupcomplete.ps1 -OutFile C:\Windows\Setup\Scripts\Setupcomplete.ps1
$Error.Exception.Message >> $log

###0- Execution du role############################################################################################

powershell.exe -file C:\Windows\Setup\Scripts\Setupcomplete.ps1