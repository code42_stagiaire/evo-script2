###################################################################################################################
#
#   nom du script   : MAJ.PS1
#   description     : Permet de  
#                           30- Montage du lecteur reseau (Partage WSUS-SERVEUR)
#                           31- Ajout du fichier SYSPREP.CMD dans le dossier Startup
#                           32- Mise a jour de windows via un depot wsusoffline (autoreboot)
#                           33- Redémarrage de la VM
#   teste sur       : Windows 7,10,2008R2,2012R2,2016
#   auteur          : Geoffroy CORBINEAU
#   statut          : Développement
#   version         : 0.7
#   
#
###################################################################################################################


###################################################################################################################
#
#   Variables
#
###################################################################################################################

$wsus = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.wsus" 2>&1 | Out-String
$wsus = $wsus -replace "`t|`n|`r",""

$log = "C:\SCRIPTS\Logs\logs.txt"


###################################################################################################################
#
#   Main
#
###################################################################################################################

###30- Montage du lecteur reseau###################################################################################

$Error.clear()
echo "###MAJ.PS1###################" >> $log 
echo "30- Montage du lecteur reseau :" >> $log 
New-PSDrive -name W -Root $wsus -PSProvider FileSystem -Persist
$Error.Exception.Message >> $log 

###31- Ajout du fichier SYSPREP.CMD dans le dossier Startup########################################################

$Error.clear()
echo "31- Ajout du fichier SYSPREP.CMD dans le dossier Startup" >> $log
cmd.exe /C copy C:\SCRIPTS\Scripts_PS\SYSPREP.cmd "%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\SYSPREP.cmd"
$Error.Exception.Message >> $log 

###32- Mise a jour de windows via un depot wsusoffline (autoreboot)################################################

echo "32- Mise a jour de windows via un depot wsusoffline (autoreboot)"  >> $log
#cmd.exe /C w:\cmd\DoUpdate.cmd /verify /updatecpp /updatetsc /instdotnet4 /autoreboot
$Error.Exception.Message >> $log 

###33- Redémarrage de la VM########################################################################################

echo "33- Redemarrage de la VM :" >> $log
Restart-Computer