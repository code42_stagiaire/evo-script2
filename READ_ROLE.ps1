﻿###################################################################################################################
#
#   nom du script   : READ_ROLE.PS1
#   description     : Permet de  
#                           0- Lecture des guestinfo
#                           0- Telechargement du role  
#        ####Custom.ps1 se telecharge dans c:\windows\setup car le dossier C:\SCRIPTS\Scripts_PS\ n'existe pas#####
#                           0- Execution du role
#   teste sur       : Windows 7,10,2008R2,2012R2,2016
#   auteur          : Geoffroy CORBINEAU
#   statut          : Développement
#   version         : 0.7
#   
#
###################################################################################################################


###0- Lecture des guestinfo########################################################################################

$role = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.role" 2>&1 | Out-String
$role = "$role.ps1" -replace "`t|`n|`r",""
$addresse = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.addresse" 2>&1 | Out-String
$addresse = $addresse -replace "`t|`n|`r",""
 
###0- Telechargement du role#######################################################################################

#Invoke-WebRequest $addresse/$role -outfile c:\windows\setup\$role
Invoke-WebRequest $addresse/$role -outfile C:\SCRIPTS\Scripts_PS\$role

###0- Execution du role############################################################################################

#powershell.exe -file c:\windows\setup\$role
powershell.exe -file C:\SCRIPTS\Scripts_PS\$role