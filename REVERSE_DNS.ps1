###################################################################################################################
#
#   nom du script   : REVERSE_DNS.PS1
#   description     : Permet de  
#                           86- Ajout d'une zone inverse
#   teste sur       : Windows 7,10,2008R2,2012R2,2016
#   auteur          : Geoffroy CORBINEAU
#   statut          : Développement
#   version         : 0.7
#   
#
###################################################################################################################


###################################################################################################################
#
#   Variables
#
###################################################################################################################

$ip_address = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.ipaddress" 2>&1 | Out-String
$ip_address = $ip_address -replace "`t|`n|`r",""
$ip_netmask = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.netmask" 2>&1 | Out-String
$ip_netmask = $ip_netmask -replace "`t|`n|`r",""
$log = "C:\SCRIPTS\Logs\logs.txt" 


###################################################################################################################
#
#   Fonctions (1- Permet de convertir ex: 192.168.5.24 255.255.255.0 en 192.168.5.0/24)
#
###################################################################################################################

function toBinary ($dottedDecimal){
 $dottedDecimal.split(".") | %{$binary=$binary + $([convert]::toString($_,2).padleft(8,"0"))}
 return $binary
}
function toDottedDecimal ($binary){
 do {$dottedDecimal += "." + [string]$([convert]::toInt32($binary.substring($i,8),2)); $i+=8 } while ($i -le 24)
 return $dottedDecimal.substring(1)
}
$ipBinary = toBinary $ip_address
$smBinary = toBinary $ip_netmask
$cidr=$smBinary.indexOf("0")
$netBits=$smBinary.indexOf("0")
$networkID = toDottedDecimal $($ipBinary.substring(0,$netBits).padright(32,"0"))

###################################################################################################################
#
#   Main
#
###################################################################################################################

$Error.clear()
echo `t "###REVERSE_DNS.PS1###########" >> $log
echo "86- Ajout d'une zone inverse :" >> $log
Add-DnsServerPrimaryZone -NetworkID $networkID/$cidr -ReplicationScope Forest
$Error.Exception.Message >> $log
