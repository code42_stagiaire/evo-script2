###################################################################################################################
#
#   nom du script   : SYSPREP.PS1
#   description     : Permet de  
#                           34- Suppression du lecteur W ( wsusoffline )
#                           35- Preparation au sysprep
#                           36- Envoie Email
#                           37- Extinction de la VM apres sysprep
#                           38- Redemarrage de la VM
#   teste sur       : Windows 7,10,2008R2,2012R2,2016
#   auteur          : Geoffroy CORBINEAU
#   statut          : Développement
#   version         : 0.7
#   
#
###################################################################################################################


###################################################################################################################
#
#   Variables
#
###################################################################################################################

$VM_name = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.hostname" 2>&1 | Out-String
$VM_name = $VM_name -replace "`t|`n|`r",""

$log  = "C:\SCRIPTS\Logs\logs.txt"


###################################################################################################################
#
#   Main
#
###################################################################################################################

###34- Suppression du lecteur W ( wsusoffline )####################################################################

$Error.clear()
echo `t "###SYSPREP.PS1###############" >> $log 
echo "34- Suppression du lecteur W ( wsusoffline ): " >> $log  
net use w: /delete
$Error.Exception.Message >> $log 

###35- Preparation au sysprep######################################################################################

$Error.clear()
echo "35- Preparation au sysprep : "  >> $log  
cmd.exe /C del /q /f "%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\SYSPREP.cmd"
#New-ItemProperty -Path HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce sysprep -Value 'c:\windows\system32\sysprep\sysprep.exe /generalize /oobe /reboot /unattend:"C:\Windows\System32\sysprep\unattend.xml"'  
New-ItemProperty -Path HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce sysprep -Value 'c:\windows\system32\sysprep\sysprep.exe /generalize /oobe /shutdown /unattend:"C:\Windows\System32\sysprep\unattend.xml"'  
$Error.Exception.Message >> $log 

###36- Envoie Email################################################################################################

echo "37- Extinction de la VM apres sysprep :" >> $log
echo "38- Redemarrage de la VM :" >> $log
date >> $log
$logs = cat $log  
$email = @{
                    To = "stagiaire@code42.fr","alerte@code42.fr"
                    From = "evo@code42.io"
                    Subject = "[EVO] - CONfiguration Modele rapport -> $VM_name"
                    Body = ( $logs | Out-String )
                    SmtpServer = "f.code42.fr"
                    port = "25"
               }
                    Send-MailMessage @email 

###38- Redémarrage de la VM########################################################################################

Restart-Computer

