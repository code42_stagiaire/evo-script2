###################################################################################################################
#
#   nom du script   : SETUPCOMPLETE.PS1
#   description     : Permet de  
#                           40- Envoyer les guestinfo dans logs.txt
#                           41- Si nom de VM nul ou trop long, DC-1 par default
#                           42- Installation MINION & ZABBIX
#                           43- Configuration reseaux
#                           44- DVD en Derniere lettre dispo
#                           45- Cr�ation des comptes et d�sactivation compte administrateur
#                           46- Lecture seule pour les non GRP-CODE42 au dossier SCRIPTS et INSTALL
#                           47- Suppression des scripts + icone adobe reader
#                           48- Auto Logon
#                           49- Changer port par default RDP
#                           50- Regles de Pare-Feu 
#                           51- Desactiver UAC
#                           52- Nommage 1�re partition C:SYSTEME
#                           53- Activation RDP sans NLA
#                           54- BGInfo lanc� au d�marrage avec le bon template
#                           55- Installation Client Telnet, SNMP
#                           56- Desactiver mise en veille
#                           59- Insertion Read_Role dans runonce
#                           #################TEST################# (A REVOIR)
#                           60- R�cuperation du sid
#                           61- R�cup�ration information r�seaux et OS
#                           62- Test ping GOOGLE.FR et CODE42.FR
#                           63- Test telnet sur les ports sp�cifique
#                           69- Envoie Email
#                           70- Redemarrage de la VM
#   teste sur       : Windows 7,10,2008R2,2012R2,2016
#   auteur          : Geoffroy CORBINEAU
#   statut          : D�veloppement
#   version         : 0.7
#   
#
###################################################################################################################


###################################################################################################################
#
#   Variables
#
###################################################################################################################

$uid = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.uid" 2>&1 | Out-String
$uid = $uid -replace "`t|`n|`r",""
$email = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.email" 2>&1 | Out-String
$email = $email -replace "`t|`n|`r",""
$ip_address = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.ipaddress" 2>&1 | Out-String
$ip_address = $ip_address -replace "`t|`n|`r",""
$ip_gateway = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.gateway" 2>&1 | Out-String
$ip_gateway = $ip_gateway -replace "`t|`n|`r",""
$ip_netmask = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.netmask" 2>&1 | Out-String
$ip_netmask = $ip_netmask -replace "`t|`n|`r",""
$VM_name = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.hostname" 2>&1 | Out-String
$VM_name = $VM_name -replace "`t|`n|`r",""
$addresse = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.addresse" 2>&1 | Out-String
$addresse = $addresse -replace "`t|`n|`r",""
$role = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.role" 2>&1 | Out-String
$role = "$role.ps1" -replace "`t|`n|`r",""
$salt_master = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.salt_master" 2>&1 | Out-String
$salt_master = $salt_master -replace "`t|`n|`r",""
$ip_dns1 = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.dns1" 2>&1 | Out-String
$ip_dns1 = $ip_dns1 -replace "`t|`n|`r",""
$ip_dns2 = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.dns2" 2>&1 | Out-String
$ip_dns2 = $ip_dns2 -replace "`t|`n|`r",""
$rdp = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.rdp" 2>&1 | Out-String
$rdp = $rdp -replace "`t|`n|`r",""
$os = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.os" 2>&1 | Out-String
$os = $os -replace "`t|`n|`r",""
$sources = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.sources" 2>&1 | Out-String
$sources = $sources -replace "`t|`n|`r",""
$wsus = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.wsus" 2>&1 | Out-String
$wsus = $wsus -replace "`t|`n|`r",""
$adminpass = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.adminpass" 2>&1 | Out-String
$adminpass = $adminpass -replace "`t|`n|`r",""
#$adminpass = ConvertTo-SecureString -AsPlainText -String $adminpass -Force
$admin42pass = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.admin42pass" 2>&1 | Out-String
$admin42pass = $admin42pass -replace "`t|`n|`r",""
#$admin42pass = ConvertTo-SecureString -AsPlainText -String $admin42pass -Force
$administrateurpass = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.administrateurpass" 2>&1 | Out-String
$administrateurpass = $administrateurpass -replace "`t|`n|`r",""
#$administrateurpass = ConvertTo-SecureString -AsPlainText -String $administrateurpass -Force

$log = "C:\SCRIPTS\Logs\logs.txt" 
$salt = "Salt-Minion-latest.exe"
$zabbix = "zabbix_agent-latest.msi"


###################################################################################################################
#
#   Main
#
###################################################################################################################

###40- Envoyer les guestinfo dans logs.txt#########################################################################

$Error.clear()
echo `t "###SETUPCOMPLETE.PS1#########" >> $log
echo "40- Lecture des guestinfo : " `t `
"guestinfo.uid = $uid" `
"guestinfo.email = $email" `
"guestinfo.ipaddress = $ip_address" `
"guestinfo.gateway = $ip_gateway" `
"guestinfo.netmask = $ip_netmask" `
"guestinfo.hostname = $VM_name" `
"guestinfo.addresse = $addresse" `
"guestinfo.role = $role" `
"guestinfo.salt_master = $salt_master" `
"guestinfo.ip_dns1 = $ip_dns1" `
"guestinfo.ip_dns2 = $ip_dns2" `
"guestinfo.rdp = $rdp" `
"guestinfo.os = $os" `
"guestinfo.sources = $sources" `
"guestinfo.wsus = $wsus" `
"guestinfo.adminpass = *********" `
"guestinfo.admin42pass = *********" `
"guestinfo.administrateurpass = *********" `t >> $log
$Error.Exception.Message >> $log

###41- Si nom de VM nul ou trop long, DC-1 par default#############################################################

$Error.clear()
echo "41- Verification nom de machine :" >> $log
if($VM_name.length -gt 15 -Or $VM_name.length -eq 0)
{
	Write-Host "`n Le nom de la VM doit �tre compris entre 1 et 11 caract�res`r`n Assignation du nom par d�faut : DC-1" >> $log
	$VM_Name = "DC-1"
}
$Error.Exception.Message >> $log

###42- INSTALL MINION & ZABBIX#####################################################################################

$Error.clear()
echo "42- INSTALL MINION & ZABBIX :" >> $log
cmd.exe /C "C:\INSTALL\SOURCES\$salt" /S /master="$salt_master" /minion-name=$VM_name | Out-Null
cmd.exe /C "msiexec /I C:\INSTALL\SOURCES\$zabbix HOSTNAME=$VM_name SERVER=40.76.58.143 LPORT=10050 RMTCMD=1 /qn" | Out-Null
net start salt-minion
$Error.Exception.Message >> $log

###43- CHANGE NETWORK CONFIGURATION################################################################################

$Error.clear()
echo "43- CHANGE NETWORK CONFIGURATION :" >> $log
If (($os -match "WIN7") -or ($os -match "WIN2008")) {
cmd.exe /C "c:\windows\system32\netsh.exe interface ip set address ""Connexion au r�seau local"" static $ip_address $ip_netmask $ip_gateway 1"
cmd.exe /C "c:\windows\system32\netsh.exe interface ip set dnsservers ""Connexion au r�seau local"" static $ip_dns1 primary"
cmd.exe /C "c:\windows\system32\netsh.exe interface ip add dnsservers ""Connexion au r�seau local"" $ip_dns2"
}
If (($os -match "WIN10") -or ($os -match "WIN2012") -or ($os -match "WIN2016")) {
cmd.exe /C "c:\windows\system32\netsh.exe interface ip set address ""Ethernet"" static $ip_address $ip_netmask $ip_gateway 1"
cmd.exe /C "c:\windows\system32\netsh.exe interface ip set dnsservers ""Ethernet"" static $ip_dns1 primary"
cmd.exe /C "c:\windows\system32\netsh.exe interface ip add dnsservers ""Ethernet"" $ip_dns2"
}
$Error.Exception.Message >> $log

###44- DVD en Derniere lettre dispo################################################################################

$Error.clear()
echo "44- DVD en Derniere lettre dispo:" >> $log
$CDROMs = @(Get-WmiObject Win32_Volume -Filter "DriveType=5")
$Alpha = @()
65..90 | % { $Alpha += [char]$_ }
for ($x=0; $x -lt $CDROMs.Count; $x++) {
    $idx = 25 - $x
    $CDROMs[$x] | Set-WmiInstance -Arguments @{DriveLetter = "$($Alpha[$idx])`:" }
} 
$Error.Exception.Message >> $log

###45- Cr�ation des comptes et desactivation compte administrateur#################################################

$Error.clear()
echo "45- Creation des comptes et desactivation compte administrateur :" >> $log
net localgroup GRP-CODE42 /comment:"Groupe Code42" /add
net user admin $adminpass /add
net user admin42 $admin42pass /add
net localgroup administrateurs /add admin
net localgroup administrateurs /add admin42
net localgroup GRP-CODE42 /add administrateur
net localgroup GRP-CODE42 /add admin42
net user administrateur $administrateurpass
net user administrateur /active:no
$Error.Exception.Message >> $log

###46- Droit d�acc�s accessible en lecteur seule pour les non-GRP-CODE42 � SCRIPTS et INSTALL######################

$Error.clear()
echo "46- Droit d acces accessible en lecteur seule pour les non-GRP-CODE42 a SCRIPTS et INSTALL :" >> $log
cacls C:\INSTALL /E /R administrateurs /t
cacls C:\INSTALL /E /R "CREATEUR PROPRIETAIRE" /t
cacls C:\INSTALL /E /R "Utilisateurs authentifi�s" /t
cacls C:\SCRIPTS /E /R administrateurs /t
cacls C:\SCRIPTS /E /R "CREATEUR PROPRIETAIRE" /t
cacls C:\SCRIPTS /E /R "Utilisateurs authentifi�s" /t
cacls C:\SCRIPTS\Logs /E /G admin:F /t 
cacls C:\INSTALL /E /G GRP-CODE42:F /t
cacls C:\SCRIPTS /E /G GRP-CODE42:F /t
$Error.Exception.Message >> $log

###47- Suppression des scripts + icones############################################################################

$Error.clear()
echo "47- Suppression des scripts + icones :" >> $log
New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Run deldesktopini -Value 'cmd.exe /C "del /a /q /f %USERPROFILE%\desktop\desktop.ini"'
cmd.exe /C "del /q /f c:\windows\setup\Setupcomplete.*"
cmd.exe /C "del /q /f c:\windows\system32\sysprep\unattend.xml" 
cmd.exe /C "del /q /f c:\windows\panther\unattend.xml"
cmd.exe /C "del /q /f c:\Users\Public\Desktop\Acrobat*.lnk"
cmd.exe /C "del /q /f c:\Users\Public\Desktop\Cygwin*.lnk"
cmd.exe /C "del /a /q /f c:\Users\Public\Desktop\Desktop.ini"
cmd.exe /C "del /a /q /f c:\Users\Default\Desktop\Desktop.ini"
$Error.Exception.Message >> $log

###48- Auto Logon##################################################################################################

$Error.clear()
echo "48- Autologon :" >> $log
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "AutoAdminLogon" -Value "1" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultDomainName" -Value ".\" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultUserName" -Value "admin" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultPassword" -Value $adminpass -Force
$Error.Exception.Message >> $log


###49- Changer port par default RDP################################################################################

$Error.clear()
echo "49- Changer port par default RDP :" >> $log
Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Control\Terminal*Server\WinStations\RDP-TCP\ -Name PortNumber -Value "$rdp"
$Error.Exception.Message >> $log

###50- R�gles de Pare-Feu###########################################################################################

$Error.clear()
echo "50- Regles de Pare-Feu :" >> $log
If (($os -match "WIN10") -or ($os -match "WIN2012") -or ($os -match "WIN2016")) {
Enable-NetFirewallRule -DisplayGroup "Bureau � Distance*"
New-NetFirewallRule -Name "RDP" -DisplayName "RDP" -Protocol TCP -LocalPort $rdp
New-NetFirewallRule -Name "SMB445" -DisplayName "SMB445" -Protocol TCP -LocalPort 445
New-NetFirewallRule -Name "WINRM5986" -DisplayName "WINRM5986" -Protocol TCP -LocalPort 5986
#Set-Item (dir wsman:\localhost\Listener\*\Port -Recurse).pspath 5986 -Force
Restart-Service winrm 
}
If (($os -match "WIN7") -or ($os -match "WIN2008")) {
netsh advfirewall firewall add rule name=�RDP� dir=in action=allow protocol=TCP localport=$rdp
netsh advfirewall firewall add rule name=�SMB445� dir=in action=allow protocol=TCP localport=445
netsh advfirewall firewall add rule name=�WINRM5986� dir=in action=allow protocol=TCP localport=5986
}
$Error.Exception.Message >> $log

###51- Desactiver UAC##############################################################################################

If (($os -match "WIN7") -or ($os -match "WIN2008") -or ($os -match "WIN2012")) {
$Error.clear()
echo "51- Desactiver UAC :" >> $log
New-ItemProperty -Path HKLM:Software\Microsoft\Windows\CurrentVersion\policies\system -Name EnableLUA -PropertyType DWord -Value 0 -Force
$Error.Exception.Message >> $log
}

###52- Nommage 1�re partition C:SYSTEME############################################################################

$Error.clear()
echo "52- Nommage 1ere partition C:SYSTEME :" >> $log 
label C:SYSTEME
$Error.Exception.Message >> $log

###53- Activation RDP sans NLA#####################################################################################

$Error.clear()
echo "53- Activation RDP sans NLA :" >> $log
set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server'-name "fDenyTSConnections" -Value 0
set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name "UserAuthentication" -Value 0  
$Error.Exception.Message >> $log
 
###54- BGInfo lanc� au d�marrage avec le bon template##############################################################

$Error.clear()
echo "54- Lance Bginfo :" >> $log
New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Run BGinfo -Value "C:\INSTALL\SysinternalsSuite\Bginfo.exe C:\INSTALL\SysinternalsSuite\Bginfo.bgi /timer:0 /accepteula"
$Error.Exception.Message >> $log

###55- Installation Client Telnet, SNMP############################################################################

$Error.clear()
echo "55- Installation Client Telnet, SNMP :" >> $log
If (($os -match "WIN2012") -or ($os -match "WIN2016")) {
Add-WindowsFeature telnet-client 
Add-WindowsFeature RSAT-SNMP 
}
If (($os -match "WIN7") -or ($os -match "WIN10") -or ($os -match "WIN2008")) {
pkgmgr /iu:"TelnetClient"
pkgmgr /iu:"SNMP" 
}
$Error.Exception.Message >> $log

###56- Desactiver mise en veille###################################################################################

$Error.clear()
echo "56- Desactiver mise en veille :" >> $log
set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Services\ACPI\Parameters' -name "Attributes" -Value 112  
$Error.Exception.Message >> $log

###59- Insertion Read_Role dans runonce############################################################################

$Error.clear()
echo "59- Insertion Read_Role dans runonce :" >> $log
New-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "Read_role" -Value "$pshome\powershell.exe -file C:\SCRIPTS\Scripts_PS\READ_ROLE.ps1" -Force
$Error.Exception.Message >> $log


###################################################################################################################
#
#   Test
#
###################################################################################################################

echo `t "###TEST######################" >> $log

###60- R�cuperation du sid#########################################################################################

C:\INSTALL\SysinternalsSuite\PsGetsid.exe /accepteula >> $log

###61- R�cup�ration information r�seaux et OS######################################################################

   $Networks = Get-WmiObject Win32_NetworkAdapterConfiguration | ? {$_.IPEnabled}
   foreach ($Network in $Networks) {
    $IPAddress  = $Network.IpAddress[0]
    $SubnetMask  = $Network.IPSubnet[0]
    $DefaultGateway = $Network.DefaultIPGateway
    $DNSServers  = $Network.DNSServerSearchOrder
    $MACAddress  = $Network.MACAddress
    $DHCPServer  = $Network.DHCPServer
    $DNSDomain  = $Network.DNSDomainSuffixSearchOrder
}

$body = @"
- Nom de la machine: $VM_name
- Les informations reseaux:
    Adresse ip de la machine : $IPAddress
    Masque de sous reseau : $SubnetMask 
    Passerelle par defaut: $DefaultGateway  
    Serveur DHCP : $DHCPServer
    Adresse physique : $MACAddress 
    Serveur DNS: $DNSServers
"@

###62- Test ping GOOGLE.FR et CODE42.FR############################################################################

$ServerName = "www.code42.fr", "www.google.fr"
foreach ($Server in $ServerName) {
        if (test-Connection $Server -Count 2 -Quiet ) {         
            $status = "`n- ping sur le serveur $Server : Succes"                                                
                    } else                    
                    { $status = "`n- ping sur le serveur $Server : Echec"                                                                  
                    }
                    $body += $status          
}

###63- Test telnet sur les ports sp�cifique######################################################################## 

$test = 'localhost:10050','s.code42.fr:10051'
Foreach ($t in $test)  
{
    $source, $port = $t.Split(':')
    #Write-Host "connection au port $port sur $source..."
    try
    {
        $socket = New-Object System.Net.Sockets.TcpClient($source, $port)?
        $status = "`n- telnet $t : Succes"
    }
    catch
    {
    }
        $status = "`n- telnet $t : Echec"
    $body += $status
}

###69- Envoie Email################################################################################################

echo $body >> $log
echo `t "70- Redemarrage de la VM :" >> $log
date >> $log
$logs = cat $log 
$email = @{
                    To = "stagiaire@code42.fr","alerte@code42.fr"
                    From = "evo@code42.io"
                    Subject = "[EVO] - CONfiguration deploiement rapport -> $VM_name"
                    Body = ( $logs | Out-String )
                    SmtpServer = "f.code42.fr"
                    port = "25"
               }
                    Send-MailMessage @email 

###70- Redemarrage de la VM########################################################################################

Rename-Computer -NewName $VM_name -Force -Restart
